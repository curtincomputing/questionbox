# Simulation of dice rolling
import random

# Function to validate number of rolls, can't be negative

def validateRolls():
    valid = False
    while not valid:
        value = input()
        if value.isdigit():
            intval = int(value)
            if intval >= 0:
                valid = True
            else:
                print("Invalid value - re-enter please...")
        else:
            print("Invalid value - re-enter please...")
    return intval

# Function to do one diceroll

def diceroll(sides=6):
    res = random.randint(1,sides)
    return res

# Ask user number of rolls and roll dice

print("Enter number of times to roll the dice...")
numrolls = validateRolls()

print("## Eight sided dice ##")
for i in range(numrolls):
    result = diceroll(8)
    print("Dice roll", i, " is: ", result)

print("## Default (six) sided dice ##")
for i in range(numrolls):
    result = diceroll()
    print("Dice roll", i, " is: ", result)
